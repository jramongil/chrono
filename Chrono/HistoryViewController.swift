//
//  HistoryViewController.swift
//  Chrono
//
//  Created by Javier Ramon Gil on 30/6/15.
//  Copyright (c) 2015 Javier Ramon. All rights reserved.
//

import UIKit

class HistoryViewController: UITableViewController {
    var historyArray :NSMutableArray!
    
    let tableIdentifier = "TableIdentifier"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadDataPlist()
        
        let app = UIApplication.sharedApplication()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "applicationWillResignActive:", name: UIApplicationWillResignActiveNotification, object: app)
        
    }
    
    func dataFilePath()->String{
        let paths = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
        let documentsDirectory = paths[0] as! NSString
        return documentsDirectory.stringByAppendingPathComponent("data.plist") as String
    }
    
    func loadDataPlist(){
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true) as NSArray
        let documentsDirectory = paths.objectAtIndex(0) as! NSString
        let path = documentsDirectory.stringByAppendingPathComponent("data.plist")
        let fileManager = NSFileManager.defaultManager()
        if !fileManager.fileExistsAtPath(path){
            let bundle = NSBundle.mainBundle().pathForResource("data", ofType: "plist")
            fileManager.copyItemAtPath(bundle!, toPath: path, error: nil)
        }
        historyArray = NSMutableArray(contentsOfFile: path)!
    }
    
    func applicationWillResignActive(notification:NSNotification){
        let filePath = self.dataFilePath()
        let array = (self.historyArray as NSArray).valueForKey("text") as! NSArray
        array.writeToFile(filePath, atomically: true)
    }

    // MARK: - Table view data source

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        return historyArray.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier(tableIdentifier, forIndexPath: indexPath) as? UITableViewCell

        // Configure the cell...
        if cell == nil {
            cell = UITableViewCell(style: .Default, reuseIdentifier: tableIdentifier)
        }
        cell!.textLabel!.text = historyArray[indexPath.row] as? String
        return cell!
    }

}
