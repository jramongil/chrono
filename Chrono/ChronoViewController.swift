//
//  ChronoViewController.swift
//  Chrono
//
//  Created by Javier Ramon Gil on 4/6/15.
//  Copyright (c) 2015 Javier Ramon. All rights reserved.
//

import UIKit

class ChronoViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet var chronoLabel: UILabel!
    @IBOutlet var startButton: UIButton!
    @IBOutlet var resetButton: UIButton!
    @IBOutlet var tableView:UITableView!
    let tableIdentifier = "TableIdentifier"
    var startTime = NSTimeInterval()
    var timer = NSTimer()
    var time:String!
    var array = [String]()
    var arrayData : NSArray!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        startButton.backgroundColor = UIColor.greenColor()
        startButton.layer.borderWidth = 1
        startButton.layer.borderColor = UIColor.blackColor().CGColor
        
        resetButton.backgroundColor = UIColor.redColor()
        resetButton.layer.borderWidth = 1
        resetButton.layer.borderColor = UIColor.blackColor().CGColor

        tableView.dataSource = self
        tableView.delegate = self
        
        array.removeAll(keepCapacity: false)
    }
    
    func updateTime(){
        var currentTime = NSDate.timeIntervalSinceReferenceDate()
        
        //Find the difference between current time and start time
        var elapsedTime: NSTimeInterval = currentTime - startTime
        
        //calculate the minutes in elapsed time
        let minutes = UInt8(elapsedTime / 60.0)
        elapsedTime -= (NSTimeInterval(minutes) * 60)
        
        //calculate the seconds in elapsed time.
        let seconds = UInt8(elapsedTime)
        elapsedTime -= NSTimeInterval(seconds)
        
        //find out the fraction of milliseconds to be displayed.
        let fraction = UInt8(elapsedTime * 100)
        
        //add the leading zero for minutes, seconds and millseconds and store them as string constants
        let strMinutes = minutes > 9 ? String(minutes):"0" + String(minutes)
        let strSeconds = seconds > 9 ? String(seconds):"0" + String(seconds)
        let strFraction = fraction > 9 ? String(fraction):"0" + String(fraction)
        
        //concatenate minutes, seconds and milliseconds as assign it to the UILabel
        chronoLabel.text = "\(strMinutes):\(strSeconds):\(strFraction)"
        
        time = chronoLabel.text
    }
    
    @IBAction func onStartPressed(sender:UIButton){
        if !timer.valid {
            let selector : Selector = "updateTime"
            timer = NSTimer.scheduledTimerWithTimeInterval(0.01, target: self, selector: selector, userInfo: nil, repeats: true)
            startTime = NSDate.timeIntervalSinceReferenceDate()
        }
        
        if time != nil{
            array.append(time)
            tableView.reloadData()
        }
        
        arrayData = array
        writeToPlist()
        
    }
    
    @IBAction func onResetPressed(sender:UIButton){
        timer.invalidate()
        chronoLabel.text = "00:00:00"
        array.removeAll(keepCapacity: false)
        time = nil
    }
    
    @IBAction func historyButtonPressed(sender:UIButton){
        performSegueWithIdentifier("showHistory", sender: nil)
        arrayData = array
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier(tableIdentifier) as? UITableViewCell
        if cell == nil {
            cell = UITableViewCell(style: .Default, reuseIdentifier: tableIdentifier)
        }
        cell!.textLabel!.text = array[array.count - 1 - indexPath.row]
        return cell!
    }
    
    func writeToPlist(){
        var paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as! String
        var path = paths.stringByAppendingPathComponent("data.plist") as String
        arrayData.writeToFile(path, atomically: true)
    }
    

}
